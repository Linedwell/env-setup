"""""" Sections "
"
"  1.General
"  2.UI
"  3.Search
"  4.Tabs&Indent
"  5.Misc 
"  4.Plugins
"
"""""""""""""""""

""" 1.General

" Set the leader key to ','
let mapleader = ","


""" 2.UI

 set t_Co=256
 syntax on
 colorscheme desert
 set background=dark

 set laststatus=2 " Always display the statusline in all windows
 set showtabline=2 " Always display the tabline, even if there is only one tab
 set noshowmode " Hide the default mode text (e.g. -- INSERT -- below the statusline)
 
 set number 
 set relativenumber
 
 " Change line numbering column color
 highlight LineNr ctermfg=grey ctermbg=black
 
 " Highlight colomn 120
  if exists('+colorcolumn')
    set colorcolumn=120
    highlight ColorColumn ctermbg=black
  else
    au BufWinEnter * let w:m2=matchadd('ErrorMsg', '\%>80v.\+', -1)
  endif


 " Highlight current line
 highlight CursorLine   cterm=NONE ctermbg=black
 set cursorline
 
 "Always show current position
 set ruler

 " Show matching brackets when text indicator is over them
 set showmatch
 " How many tenths of a second to blink when matching brackets
 set mat=2

 set visualbell

""" 3.Search
  " Ignore case when searching
  set ignorecase

  " When upper-case is typed in search, search is case-sensitive
  set smartcase


  " Highlight search results
  set hlsearch
  hi Search ctermbg=LightYellow
  hi Search ctermfg=Red

  " Move the cursor to the matched string while typing the search pattern
  set incsearch

  " Extended regular expression (as opposed to grep-like comportment)
  set nomagic

  " Visual mode pressing * or # searches for the current selection
  vnoremap <silent> * :<C-u>call VisualSelection('', '')<CR>/<C-R>=@/<CR><CR>
  vnoremap <silent> # :<C-u>call VisualSelection('', '')<CR>?<C-R>=@/<CR><CR>

  " Disable highlight when <leader><cr> is pressed
  map <silent> <leader><cr> :noh<cr>


""" 5.Misc

  " Remove the Windows ^M on leader + m
  noremap <Leader>m mmHmt:%s/<C-V><cr>//ge<cr>'tzt'm

  " Reload xrdb when Xresources is changed
  "autocmd BufWritePost ~/.Xresources !xrdb -merge %


""" 6.Plugins

" Filetype configuration
filetype plugin on
filetype plugin indent on

 "Configuration Powerline
 python3 from powerline.vim import setup as powerline_setup
 python3 powerline_setup()
 python3 del powerline_setup

